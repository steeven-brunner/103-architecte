/*
** 103architecte.c for 103architecte in /home/brunne_s/rendu/103architecte
** 
** Made by Steeven Brunner
** Login   <brunne_s@epitech.net>
** 
** Started on  Tue Dec 10 13:39:39 2013 Steeven Brunner
** Last update Fri Dec 13 13:31:24 2013 brunner steeven
*/

#include <stdlib.h>
#include <math.h>

double	**init_result()
{
  double	**matrix;
  int		l;

  l = 0;
  matrix = malloc(3 * sizeof(double *));
  if (matrix == NULL)
    return (0);
  while (l < 3)
    {
      matrix[l] = malloc(3 * sizeof(double));
      l++;
    }
  matrix[0][0] = 0;
  matrix[0][1] = 0;
  matrix[0][2] = 0;
  matrix[1][0] = 0;
  matrix[1][1] = 0;
  matrix[1][2] = 0;
  matrix[2][0] = 0;
  matrix[2][1] = 0;
  matrix[2][2] = 0;
  return (matrix);
}

double	**result_coordinate(double x, double y, double **matrix)
{
  double	**final_matrix;
  int		k;

  k = 0;
  final_matrix = malloc(3 * sizeof(double *));
  while (k < 1)
    {
      final_matrix[k] = malloc(3 * sizeof(double));
      k++;
    }
  final_matrix[0][0] = matrix[0][0] * x + matrix[0][1] * y + matrix[0][2];
  final_matrix[0][1] = matrix[1][0] * x + matrix[1][1] * y + matrix[1][2];
  final_matrix[0][2] = matrix[2][0] * x + matrix[2][1] * y + matrix[2][2];
 
  return (final_matrix);
}

double	**multip_matrix(double **matrix1, double **matrix2)
{
  int		i;
  int		j;
  int		k;
  int		l;  
  double	**result;
  double	temp;

  i = 0;
  j = 0;
  k = 0;
  l = 0;
  result = malloc(3 * sizeof(double *));
  if (result == NULL)
    return (0);
  while (l < 3)
    {
      result[l] = malloc(3 * sizeof(double));
      l++;
    }
  while (i < 3)
    {
      while (j < 3)
	{
	 temp = matrix1[i][j] * matrix2[j][i];
	 result[i][k] = result[i][j] + temp;
	  j++;
	}
      j = 0;
      k++;
      i++;
    }
  return (result);
}

double	**init_scalling(double sx, double sy)
{
  double	**matrix;
  int		i;

  i = 0;
  matrix = malloc(3 * sizeof(double *));
  if (matrix == NULL)
    return (0);
  while (i < 3)
    {
      matrix[i] = malloc(3 * sizeof(double));
      i++;
    }
  matrix[0][0] = sx;
  matrix[1][1] = sy;
  matrix[0][1] = 0;
  matrix[0][2] = 0;
  matrix[1][0] = 0;
  matrix[1][2] = 0;
  matrix[2][0] = 0;
  matrix[2][1] = 0;
  matrix[2][2] = 1;
  return (matrix);
}

double	**init_rotation(double angle)
{
  double	**matrix;
  double	cos_angle;
  double	sin_angle;
  int		i;

  i = 0;
  matrix = malloc(3 * sizeof(double *));
  if (matrix == NULL)
    return (0);
  while (i < 3)
    {
      matrix[i] = malloc(3 * sizeof(double));
      i++;
    }
  while (angle > 360)
    angle = angle - 360;
  while (angle < 0)
    angle = angle + 360;
  angle = ((angle / 180) * M_PI);
  cos_angle = cos(angle);
  sin_angle = sin(angle);
  matrix[0][2] = 0;
  matrix[1][2] = 0;
  matrix[2][0] = 0;
  matrix[2][1] = 0;
  matrix[2][2] = 1;
  matrix[0][0] = cos_angle;
  matrix[0][1] = -sin_angle;
  matrix[1][0] = sin_angle;
  matrix[1][1] = cos_angle;  
  printf("%f    %f    %f\n%f    %f    %f\n%f    %f    %f\n", matrix[0][0], matrix[0][1], matrix[0][2],  matrix[\
													       1][0],  matrix[1][1],  matrix[1][2],  matrix[2][0],  matrix[2][1], matrix[2][2]);
  return (matrix);
}

double	**init_translation(double tx, double ty)
{
  double	**matrix;
  int		i;

  i = 0;
  matrix = malloc(3 * sizeof(double *));
  if (matrix == NULL)
    return (0);
  while (i < 3)
    {
      matrix[i] = malloc(3 * sizeof(double));
      i++;
    }
  matrix[0][0] = 1;
  matrix[0][1] = 0;
  matrix[0][2] = tx;
  matrix[1][0] = 0;
  matrix[1][1] = 1;
  matrix[1][2] = ty;
  matrix[2][0] = 0;
  matrix[2][1] = 0;
  matrix[2][2] = 1;
  printf("%f    %f    %f\n%f    %f    %f\n%f    %f    %f\n", matrix[0][0], matrix[0][1], matrix[0][2],  matrix[1][0],  matrix[1][1],  matrix[1][2],  matrix[2][0],  matrix[2][1], matrix[2][2]);
  return (matrix);
}
