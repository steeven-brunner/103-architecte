/*
** 103architecte.c for 103architecte in /home/brunne_s/rendu/103architecte
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Tue Dec 10 15:08:09 2013 brunner steeven
** Last update Fri Dec 13 13:32:59 2013 brunner steeven
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int	main(int argc, char **argv)
{
  int		i;
  double	coo_1;
  double	coo_2;
  double       	angle_rot;
  double       	sym;
  double	tx;
  double	ty;
  double	sx;
  double	sy;
  double	**matrix;
  int		k;

  k = 0;
  matrix = malloc(3 * sizeof(double *));
    while (k < 3)
      {
        matrix[k] = malloc(3 * sizeof(double));
        k++;
      }
  i = 3;
  if (argc < 3)
    {
      printf("Erreur : il n'y a pas assez d'arguments !\n");
      return (0);
    }
  coo_1 = atoi(argv[1]);
  coo_2 = atoi(argv[2]);  
  while (i < argc)
    {
      if (argv[i][0] == 'T' && argv[i][1] == '\0')
	{
	  if ((i + 1 == argc || i + 2 == argc || ((argv[i + 1][0] < '0' || argv[i + 1][0] > '9') && argv[i + 1][0] != '-'))
	      || ((argv[i + 2][0] < '0' || argv[i + 2][0] > '9') && argv[i + 2][0] != '-'))
            {
              printf("Erreur : arguments manquants !\n");
              return (0);
            }

	  printf("translation de vecteur (%d,%d)\n", atoi(argv[i + 1]), atoi(argv[i + 2]));
	  tx = atoi(argv[i + 1]);
	  ty = atoi(argv[i + 2]);
	  matrix = init_translation(tx, ty);
	  matrix = (coo_1, coo_2, matrix);
	  printf("%.3f\n", matrix[1][0]);
	}
      if (argv[i][0] == 'H' && argv[i][1] == '\0')
	{
	  if ((i + 1 == argc || i + 2 == argc || ((argv[i + 1][0] < '0' || argv[i + 1][0] > '9') && argv[i + 1][0] != '-'))
	      || ((argv[i + 2][0] < '0' || argv[i + 2][0] > '9') && argv[i + 2][0] != '-'))
            {
              printf("Erreur : arguments manquants !\n");
	      return (0);
            }
	  printf("homothétie de rapports %d et %d\n", atoi(argv[i + 1]), atoi(argv[i + 2]));
	  sx = atoi(argv[i + 1]);
	  sy = atoi(argv[i + 2]);
	  init_scalling(sx, sy);
	}
      if (argv[i][0] == 'R' && argv[i][1] == '\0')
	{	
	  if (i + 1 == argc || ((argv[i + 1][0] < '0' || argv[i + 1][0] > '9') && argv[i + 1][0] != '-'))
	    {
	      printf("Erreur : arguments manquants !\n");
	      return (0);
	    }
	  printf("rotation d'angle %d degrés\n", atoi(argv[i + 1]));
	  angle_rot = atoi(argv[i + 1]);
	  matrix = init_rotation(angle_rot);
	  matrix = (coo_1, coo_2, matrix);
	  printf("%.3f\n", matrix[0][0]);
	}
      if (argv[i][0] == 'S' && argv[i][1] == '\0')
	{
	  if (i + 1 == argc || ((argv[i + 1][0] < '0' || argv[i + 1][0] > '9') && argv[i + 1][0] != '-'))
            {
              printf("Erreur : arguments manquants !\n");
              return (0);
            }
	  printf("symétrie par rapport à un axe incliné de %d degrés\n", atoi(argv[i + 1]));
          sym = atoi(argv[i + 1]);
        }
      i++;
    }
  return (0);
}
